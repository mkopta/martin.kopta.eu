#!/usr/bin/env python3
import os
import sys

PICTURE_PAGE_TEMPLATE = \
'''<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Martin Kopta / Foto / %(directory_name)s / %(picture_shortname)s</title>
    <link rel="stylesheet" type="text/css" href="../picture.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div class="picture">
        <a href="%(next_picture_name)s.html">
            <img src="pics/%(picture_name)s.jpg">
        </a>
    <p>
    </p>
    </div>
</body>
</html>'''

GALLERY_PAGE_TEMPLATE = \
'''<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Martin Kopta / Foto / %(directory_name)s</title>
    <link rel="stylesheet" type="text/css" href="../gallery.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <header>
    <h1>
        <a href="../../index.html">Martin Kopta</a>
        / <a href="../index.html">Foto</a>
        / %(directory_name)s</h1>
    <p>
%(description)s
    </p>
    </header>
    <div class="thumbnails">
%(thumbnails)s
    </div>
    <footer>
    <p>
%(technical)s
    </p>
    </footer>
</body>
</html>'''


def check_directory(directory_name):
    if not os.path.exists(directory_name):
        print('Directory "%s" does not exist.' % directory_name)
        sys.exit(1)
    if not os.path.isdir(directory_name):
        print('Directory "%s" is not a directory.' % directory_name)
        sys.exit(1)
    if not os.path.exists(directory_name + '/pics'):
        print('Directory "%s/pics" does not exist.' % directory_name)
        sys.exit(1)
    if not os.path.isdir(directory_name + '/pics'):
        print('Directory "%s/pics" is not a directory.' % directory_name)
        sys.exit(1)


def list_pictures(directory_name):
    pictures = os.listdir(directory_name + '/pics')
    if not pictures:
        print('No pictures found in "%s/pics".' % directory_name)
        sys.exit(1)
    for p in pictures:
        if not p.endswith('.jpg'):
            print('Invalid suffix of a picture "%s".' % p)
            sys.exit(1)
    return sorted(pictures)


def render_page(directory_name, picture_filename, next_picture_filename):
    values = {
        'directory_name': directory_name,
        'picture_name': picture_filename.split('.')[0]
    }
    if next_picture_filename:
        values['next_picture_name'] = next_picture_filename.split('.')[0]
    else:
        values['next_picture_name'] = 'index'
    if values['picture_name'].startswith(directory_name):
        shortname = values['picture_name'][len(directory_name):]
        values['picture_shortname'] = shortname.lstrip('-').lstrip('0')
    else:
        values['picture_shortname'] = values['picture_name']
    return PICTURE_PAGE_TEMPLATE % values


def render_gallery(directory_name, pictures):
    thumbnails = ''
    for picture_filename in pictures:
        name = picture_filename.split('.')[0]
        thumbnail = f'<a href="{name}.html"><img src="pics/{name}.jpg"></a>\n'
        thumbnails += thumbnail
    values = {
        'directory_name': directory_name,
        'description': '',
        'technical': '',
        'thumbnails': thumbnails
    }
    for value in ('description', 'technical'):
        if os.path.isfile(f'{directory_name}/{value}.txt'):
            with open(f'{directory_name}/{value}.txt', 'r') as f:
                values[value] = f.read().strip()
    return GALLERY_PAGE_TEMPLATE % values


def create_htmls(directory_name, pictures):
    page = render_gallery(directory_name, pictures)
    html = f'{directory_name}/index.html'
    with open(html, 'w') as f:
        f.write(page)
    while pictures:
        picture_filename = pictures.pop(0)
        if pictures:
            next_picture_filename = pictures[0]
        else:
            next_picture_filename = None
        page = render_page(
            directory_name, picture_filename, next_picture_filename
        )
        html = f'{directory_name}/{picture_filename.split(".")[0]}.html'
        with open(html, 'w') as f:
            f.write(page)


def main():
    if len(sys.argv) != 2:
        print('Usage: %s directory' % sys.argv[0])
        sys.exit(1)
    directory_name = sys.argv[1]
    check_directory(directory_name)
    pictures = list_pictures(directory_name)
    create_htmls(directory_name, pictures)


if __name__ == '__main__':
    main()
