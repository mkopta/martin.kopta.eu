let sweep = 0;
let counter = 0;
let understanding_width = 0;
let margin = 0;
let touching = false;
let visible_sfx = null;
let dark_mode = false;
let celebrated = false;
let lavi = {x: 0, y: 0, w: 0, h: 0, warmth: []}  // left human
let reon = {x: 0, y: 0, w: 0, h: 0, warmth: []}  // right human
/* A human has warmth, which comes from understanding. */


function preload() {
    /* Load sound effects (sfx). */
    sfx_connected = loadSound('audio/connected.wav');
    sfx_perfect = loadSound('audio/perfect.wav');
    sfx_confused = loadSound('audio/confused.wav');
    sfx_understood = loadSound('audio/understood.wav');
}

function setup() {
    /* Scale canvas for current window width, but at most 400 px.
       This makes it work on small screens too.
       Place the canvas to a <div id="illustration">. */
    let canvas_width = min(windowWidth, 400);
    let canvas = createCanvas(canvas_width, 400);
    canvas.parent('illustration');
    /* Scale dimensions of objects by the canvas size. */
    understanding_width = 0.05 * width;
    margin = ((0.05 * width) + (0.1 * height)) / 2;
    lavi.x = lavi.y = reon.y = margin;
    reon.x = width - margin - reon.w;
    lavi.w = reon.w = width * 0.1
    lavi.h = reon.h = height - 2 * margin;
    randomizeUnderstanding();
}

function draw() {
    drawLayout();
    getColorMode();
    /* Wake up audio. */
    getAudioContext().resume();
    if (getAudioContext().state !== 'running') {
        /* Some browsers prevent audio until user interacts with the page. */
        fill(dark_mode ? 255 : 0);
        noStroke();
        textAlign(CENTER);
        text('click, touch or press space', width / 2, height / 2);
        return;
    }

    /* Figure out total width of lavi and reon when connected,
       so we know the edges of their bounding box.
       When lavi and reon enter completely their bounding box,
       they are connected. */
    let max_common_understanding = 0;
    for (let i in lavi.warmth) {
        // furthest reaching understanding will make the connection
        max_common_understanding = max(
            max_common_understanding,
            lavi.warmth[i] + reon.warmth[i]
        );
    }
    let w = lavi.w + reon.w + max_common_understanding * understanding_width;
    let x1 = width / 2 - w / 2;  // left edge of bounding box
    let x2 = width / 2 + w / 2;  // right edge of bounding box

    /* Move lavi and reon using the sweeping line
       from their initial position to their position in the bounding box. */
    lavi.x = constrain(sweep - w/2, margin, x1);
    reon.x = constrain(
        width - sweep - reon.w + w/2,
        x2 - reon.w,
        width - reon.w - margin
    );

    /* See if connection happened. */
    if (lavi.x == x1) {                   // lavi and reon are connected
        if (touching == false) {          // they weren't connected until now
            counter = 0;                  // start counter
            if (isPerfectConnection()) {
                playSFX('perfect');
                celebrated = true;
            } else {
                playSFX('connected');
            }
        }
        touching = true;
        if (counter > 30) {  // ~30 seconds after connection
            understand();    // try to reinforce the connection
            counter = 0;     // restart counter
        }
    } else {
        touching = false;
    }

    drawLavi();
    drawReon();
    drawSFX();

    sweep += 0.15 * deltaTime;     // move the sweeping line
    counter += deltaTime * 0.001;  // increment counter second-wise

    /* Restart the illustration on interaction. */
    if ((mouseIsPressed && mouseButton === LEFT) || keyIsDown(32)) {
        sweep = margin + lavi.w;  // start the animation a little bit sooner
        randomizeUnderstanding();
        counter = 0;
        celebrated = false;
    }
}

function isPerfectConnection() {
    /* Perfect connection is when warmth is equal,
       i.e. amount of understanding is the same. */
    zeroth_understandings = lavi.warmth[0] + reon.warmth[0];
    for (i = 1; i < lavi.warmth.length; i++) {
        if ((lavi.warmth[i] + reon.warmth[i]) != zeroth_understandings) {
            return false;
        }
    }
    return true;
}

function understand() {
    /* Attempts to understand can sometimes lead to confusion. */
    if (random(1) < 0.1) {
        confuse();
        return;
    }
    /* Find incomplete understandings (connection gaps). */
    let max_common_understanding = 0;
    for (let i in lavi.warmth) {
        max_common_understanding = max(
            max_common_understanding,
            lavi.warmth[i] + reon.warmth[i]
        );
    }
    gaps = [];
    for (i in lavi.warmth) {
        if (lavi.warmth[i] + reon.warmth[i] != max_common_understanding) {
            gaps.push(i);
        }
    }
    if (gaps.length == 0) {
        return;  // there are no gaps in the connection
    }
    /* Pick a random understanding line with a gap to fill try to balance
       the understanding (both sides must reach out). */
    gap = random(gaps);
    if (lavi.warmth[gap] == reon.warmth[gap]) {
        if (random(1) > 0.5) {
            lavi.warmth[gap] += 1;
        } else {
            reon.warmth[gap] += 1;
        }
    } else if (lavi.warmth[gap] < reon.warmth[gap]) {
        lavi.warmth[gap] += 1;
    } else {
        reon.warmth[gap] += 1;
    }
    /* After filling a gap, play a sound. */
    if (celebrated === false && isPerfectConnection()) {
        playSFX('perfect');
        celebrated = true;
    } else {
        playSFX('understood');
    }
}

function confuse() {
    /* Attempt to erase an understanding. */
    i = int(random(lavi.warmth.length))
    if (random(1) > 0.5) {
        if (lavi.warmth[i] > 0) {
            lavi.warmth[i] -= 1;
            playSFX('confused');
        }
    } else {
        if (reon.warmth[i] > 0) {
            reon.warmth[i] -= 1
            playSFX('confused');
        }
    }
}

function drawLayout() {
    background(dark_mode ? 0 : 255);
    stroke(dark_mode ? 255 : 0);
    strokeWeight(2);
    fill(dark_mode ? 0 : 255);
    rect(0, 0, width, height);
}

function drawLavi() {
    strokeWeight(2);
    stroke(dark_mode ? 0 : 255);
    fill(dark_mode ? 255 : 0);
    rect(lavi.x, lavi.y, lavi.w, lavi.h);
    let understanding_height = lavi.h / lavi.warmth.length;
    for (i in lavi.warmth) {
        for (let j = 0; j < lavi.warmth[i]; j++) {
            rect(
                lavi.x + lavi.w + understanding_width * j,
                lavi.y + understanding_height * i,
                understanding_width,
                understanding_height
            );
        }
    }
}

function drawReon() {
    strokeWeight(2);
    stroke(dark_mode ? 0 : 255);
    fill(dark_mode ? 255 : 0);
    rect(reon.x, reon.y, reon.w, reon.h);
    let understanding_height = reon.h / reon.warmth.length;
    for (i in reon.warmth) {
        for (let j = 0; j < reon.warmth[i]; j++) {
            rect(
                reon.x - understanding_width * (j + 1),
                reon.y + understanding_height * i,
                understanding_width,
                understanding_height);
        }
    }
}

function playSFX(name) {
    switch (name) {
        case 'connected':
            sfx_connected.play();
            break;
        case 'perfect':
            sfx_perfect.play();
            break;
        case 'understood':
            sfx_understood.play();
            break;
        case 'confused':
            sfx_confused.play();
            break;
    }
    visible_sfx = name;
}

function drawSFX() {
    if (visible_sfx == null) {
        return;
    }
    fill(dark_mode ? 255 : 0);
    noStroke();
    textAlign(CENTER);
    text(visible_sfx, width / 2, height - 10);
    if (counter > 1) {  // hide the visible SFX after one second
        visible_sfx = null;
    }
}

function getColorMode() {
    if (
        window.matchMedia
        && window.matchMedia('(prefers-color-scheme: dark)').matches
    ) {
        dark_mode = true;
    } else {
        dark_mode = false;
    }
}

function randomizeUnderstanding() {
    lavi.warmth = []
    reon.warmth = []
    for (i = 0; i <= int(random(2, 5)); i++) {
        lavi.warmth[i] = int(random(4));
        reon.warmth[i] = int(random(4));
    }
}
